﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Stage1 : MonoBehaviour
{

    public Image dialogueBox;
    public Text Name;
    public Text Body;
    public Image ID;

    public Image MainCharacter;
    public GameObject Pencils;

    public GameObject Chatbubble;
    public GameObject pencil1;
    public GameObject pencil2;

    private int ChatCounter;

    public Image MouseClick;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            ChatCounter++;
            if (ChatCounter == 1)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "Huh?";
            }
            if (ChatCounter == 2)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "Where am I?";
            }
            if (ChatCounter == 3)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "What is my name?";
            }
            if (ChatCounter == 3)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Name.text = "????";
                Body.text = "*I feel so Dizzy*";
            }
            if (ChatCounter == 4)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "Hmm";
            }
            if (ChatCounter == 5)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "What a strange looking room";
            }
            if (ChatCounter == 6)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "Huh?";
            }
            if (ChatCounter == 7)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "There's Something in my pocket";
            }
            if (ChatCounter == 8)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "ID?";
            }
            if (ChatCounter == 9)
            {
                dialogueBox.gameObject.SetActive(false);
                Chatbubble.gameObject.SetActive(false);
                Name.text = "????";
                Body.text = "ID?";
                ID.gameObject.SetActive(true);
            }
            if (ChatCounter == 10)
            {
                ID.gameObject.SetActive(false);
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "Alceste... Is that my name?";
            }
            if (ChatCounter == 11)
            {
                ID.gameObject.SetActive(false);
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "And there's two pair of pencils";
            }
            if (ChatCounter == 12)
            {
                ID.gameObject.SetActive(false);
                dialogueBox.gameObject.SetActive(true);
                pencil1.gameObject.SetActive(true);
                pencil2.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "Woah!    They are Floating!";
            }
            if (ChatCounter == 13)
            {
                ID.gameObject.SetActive(false);
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                MainCharacter.gameObject.SetActive(false);
                Pencils.gameObject.SetActive(true);
                Name.text = "Pencils";
                Body.text = "So you have awaken";
            }
            if (ChatCounter == 14)
            {
                ID.gameObject.SetActive(false);
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "And they can talk!";
            }
            if (ChatCounter == 15)
            {
                ID.gameObject.SetActive(false);
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Pencils.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(false);
                Name.text = "pencils";
                Body.text = "Please don't be scared, for we are your friend";
            }
            if (ChatCounter == 16)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(true);
                Name.text = "????";
                Body.text = "How can I stay calm after seeing you two?!";
            }
            if (ChatCounter == 17)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Pencils.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(false);
                Name.text = "Pencils";
                Body.text = "We are here to guide you on your way out master Alceste";
            }
            if (ChatCounter == 18)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(true);
                Name.text = "Alceste";
                Body.text = "So I am Alceste";
            }
            if (ChatCounter == 19)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Pencils.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(false);
                Name.text = "Pencils";
                Body.text = "We are *Left & Right*";
            }
            if (ChatCounter == 20)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Name.text = "Left & Right";
                Body.text = "We will be your sword and shield";
            }
            if (ChatCounter == 21)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(true);
                Name.text = "Alceste";
                Body.text = "You guys are just pencils, how can you be swords and shields?!";
            }
            if (ChatCounter == 22)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Pencils.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(false);
                Name.text = "Gauche & Droite";
                Body.text = "We are indeed pencils, but with your mathematical knowledge";
            }
            if (ChatCounter == 23)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                Name.text = "Left & Right";
                Body.text = "We can be the most helpful thing you could ever have";
            }
            if (ChatCounter == 24)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(false);
                MainCharacter.gameObject.SetActive(true);
                Name.text = "Alceste";
                Body.text = "*All these things are making my head more tilted*";
            }
            if (ChatCounter == 25)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "Alceste";
                Body.text = "Anyways";
            }
            if (ChatCounter == 26)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Name.text = "Alceste";
                Body.text = "Lets get going!";
            }
            if (ChatCounter == 27)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Pencils.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(false);
                Name.text = "Left & Right";
                Body.text = "As you command!";
            }
            if (ChatCounter == 28)
            {
                dialogueBox.gameObject.SetActive(true);
                Chatbubble.gameObject.SetActive(true);
                Pencils.gameObject.SetActive(true);
                MainCharacter.gameObject.SetActive(false);
                Name.text = "Left & Right";
                Body.text = "Let's Start by pressing the Arrow Keys to move";
                MouseClick.gameObject.SetActive(true);
            }
            if (ChatCounter == 29)
            {
                SceneManager.LoadScene("PlayerRoom");
            }
        }
    }
}
