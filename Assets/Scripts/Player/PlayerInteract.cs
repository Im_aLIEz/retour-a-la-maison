﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerInteract : MonoBehaviour
{

    public Image dialogueBox;
    public Text Name;
    public Text Body;

    private SFXManager theSFX;

    public string Speaker;
    public string Dialogue;

    private PlayerController thePlayer;

    private int ChatCounter;

    private bool interacted;

    public bool Trigger;
    private int introCounter = 0;
    private bool introduction = true;

    public GameObject thisItem;
    public GameObject thisItem1;

    public bool WithObject;
    public GameObject TutImage;
    
    // Update is called once per frame
    void Start()
    {
        theSFX = FindObjectOfType<SFXManager>();
        thePlayer = FindObjectOfType<PlayerController>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "damage swing")
        {
            ChatCounter = 1;
        }
    }
    void Update()
    {
        
            /*
            if (introduction == true)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    introCounter++;

                }
            }
            if(introCounter == 0)
            {

                thePlayer.canMove = false;
                dialogueBox.gameObject.SetActive(true);
                Name.text = "Left & Right";
                Body.text = "Hello there Master Alceste";
            }
            else if (introCounter == 1)
            {
                Name.text = "Left & Right";
                Body.text = "This is your Room";
            }
            else if (introCounter == 2)
            {
                Name.text = "Left & Right";
                Body.text = "This is where you can Save your game progress and upgrade items";
            }
            else if (introCounter == 3)
            {
                Name.text = "Left & Right";
                Body.text = "You will return here everytime you finish a stage and get yourself a rest";
            }
            else if (introCounter == 4)
            {
                Name.text = "Left & Right";
                Body.text = "Lets get started by pressing the Arrow Keys to move";
            }
            else if (introCounter == 5)
            {
                thePlayer.canMove = true;
                dialogueBox.gameObject.SetActive(false);
                introduction = false;
                ChatCounter = 0;
            }
            */

            if (ChatCounter == 1)
        {
            dialogueBox.gameObject.SetActive(true);
            Name.text = Speaker;
            Body.text = Dialogue;
            interacted = true;
            if(WithObject)
            {
                TutImage.gameObject.SetActive(true);
                
            }
        }

        if (ChatCounter == 3)
        {
            dialogueBox.gameObject.SetActive(false);
            ChatCounter = 0;
            thePlayer.canMove = true;
            interacted = false;
            TutImage.gameObject.SetActive(false);
            if (Trigger)
            {
                Destroy(gameObject);
            }
        }

        if (interacted)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ChatCounter++;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.name == "Player")
        {
            thePlayer.IndicatorOn();
            if (Input.GetMouseButtonDown(0))
            {
                theSFX.Direction.Play();
                ChatCounter++;
                thePlayer.canMove = false;
            }
            if (Trigger == true)
            {
                theSFX.Direction.Play();
                ChatCounter++;
                thePlayer.canMove = false;
                Trigger = false;
            }
        }             
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {

            thePlayer.IndicatorOff();
        }
    }



}
