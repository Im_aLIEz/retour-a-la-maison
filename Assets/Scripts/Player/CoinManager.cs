﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour {

    public Text moneyText;
    public int currentGold;

	// Use this for initialization
	void Start () {

        if (PlayerPrefs.HasKey("Points"))
        {
            currentGold = PlayerPrefs.GetInt("Points");
        }
        else
        {
            currentGold = 0;
            PlayerPrefs.SetInt("Points", 0);
        }

        moneyText.text = "Score: " + currentGold; 

	}
	
	// Update is called once per frame
	void Update ()
    {
        currentGold = PlayerPrefs.GetInt("Points");
        moneyText.text = "Score: " + currentGold;
	}
    public void Addmoney(int goldToAdd)
    {
        currentGold = PlayerPrefs.GetInt("Points");
        currentGold += goldToAdd;
        PlayerPrefs.SetInt("Points", currentGold);
    }

}
