﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPick : MonoBehaviour {

    public int value;
    public CoinManager theCM;
    

	// Use this for initialization
	void Start () {
        theCM = FindObjectOfType<CoinManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name == "Player")
        {
            theCM.Addmoney(value);
            Destroy(gameObject);
        }
    }

}
