﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgress : MonoBehaviour {

    public GameObject cannotEnter1;
    public GameObject cannotEnter2;

    public GameObject block1;
    public GameObject block2;

    int progress;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {

        progress = PlayerPrefs.GetInt("Progress");
        
        if(progress >= 3)
        {
            block1.gameObject.SetActive(false);
            cannotEnter1.gameObject.SetActive(false);
        }
        if (progress >= 4)
        {
            block2.gameObject.SetActive(false);
            cannotEnter2.gameObject.SetActive(false);
        }
    }
}
