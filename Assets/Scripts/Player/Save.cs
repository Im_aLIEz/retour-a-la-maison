﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Save : MonoBehaviour {

    public Button saveButton;
    public Button backButton;
    private PlayerController thePlayer;
    void Start()
    {
        thePlayer = FindObjectOfType<PlayerController>();
        saveButton.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Cursor.visible = true;
                thePlayer.canMove = false;
                saveButton.gameObject.SetActive(true);
                backButton.gameObject.SetActive(true);
            }
        }
    }

    public void SaveGame()
    {

        PlayerPrefs.SetInt("PlayerLevel", Application.loadedLevel);
        saveButton.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
        thePlayer.canMove = true;
        Cursor.visible = false;
    }

    public void LoadGame()
    {
        int level = PlayerPrefs.GetInt("PlayerLevel");
        Application.LoadLevel(level);
        if(level>=9)
        {
            Application.LoadLevel("PlayerRoomLoaded");
        }
        else if(level==5)
        {
            Application.LoadLevel("PlayerRoomLoaded");
        }
        else
        {
            Application.LoadLevel(level);
        }

    }

    public void Back()
    {

        saveButton.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
        thePlayer.canMove = true;
        Cursor.visible = false;
    }
}
