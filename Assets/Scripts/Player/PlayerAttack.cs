﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour {

    int x;

    bool damaging;
    bool damagingBoss;
    public int damageToGive;

    private int currentDamage;
    private PlayerStats thePS;

    public GameObject damageBurst;
    private GameObject Enemy;
    private GameObject Boss;
    public Transform hitPoint;
    public Button Answer1;
    public Button Answer2;
    public Button Answer3;
    public Button noneOfTheAbove;
    public Image Question;
    public Image QuestionaireBG;
    public GameObject combatText1;
    private string combatOutcomeCorrect = "Correct!";
    private string combatOutcomeWrong = "Wrong!";
    private string combatOutcomeHit = "Hit!";
    private string operatorHolder;

    private int randomQuestionaire1;
    private int randomQuestionaire2;
    private int correctAnswerRNG;
    private int randomOperatorRNG;

    private int correctAnswer;
    private int randomAnswer;
    private int randomAnswer2;

    private int rngButtonholder1;
    private int rngButtonholder2;
    private int rngButtonholder3;

    public Text answerButtonText1;
    public Text answerButtonText2;
    public Text answerButtonText3;
    public Text questionText;
    public Text QuestionType;

    private int answer1RNG;
    private int answer2RNG;
    private int answer3RNG;

    private bool Answer1bool;
    private bool Answer2bool;
    private bool Answer3bool;
    private bool NFTAbool;
    private bool BossAttacked;
    public bool BossDamagerActivate;

    private bool spacePressed;

    public Text BossRandom1;
    public Text BossRandom2;
    public Text BossRandom3;
    private int BossQuestion;
    private int BossQuestionHolder;
    private int BossAnswer;
    private int BossCounter;
    public Text PlayerBossAnswer;

    public GameObject bossQuestionaire;
    // Use this for initialization
    void Start() {
        Cursor.visible = false;
        thePS = FindObjectOfType<PlayerStats>();
        Answer1.gameObject.SetActive(false);
        Answer2.gameObject.SetActive(false);
        Answer3.gameObject.SetActive(false);
        noneOfTheAbove.gameObject.SetActive(false);
        Question.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

        if (Answer1bool)
        {


            Time.timeScale = 1;
            if (answerButtonText1.text == correctAnswer.ToString())
            {


                if (damaging)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeCorrect;
                    Enemy.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(currentDamage);
                    Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
                }
                Answer1.gameObject.SetActive(false);
                Answer2.gameObject.SetActive(false);
                Answer3.gameObject.SetActive(false);
                noneOfTheAbove.gameObject.SetActive(false);
                Question.gameObject.SetActive(false);
                QuestionaireBG.gameObject.SetActive(false);
                damaging = false;
                Answer1bool = false;
                Cursor.visible = false;
                QuestionType.gameObject.SetActive(false);
            }
            else
            {
                if (damaging)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeWrong;
                }
                Answer1.gameObject.SetActive(false);
                Answer2.gameObject.SetActive(false);
                Answer3.gameObject.SetActive(false);
                noneOfTheAbove.gameObject.SetActive(false);
                Question.gameObject.SetActive(false);
                QuestionaireBG.gameObject.SetActive(false);
                damaging = false;
                Answer1bool = false;
                Cursor.visible = false;
                QuestionType.gameObject.SetActive(false);
            }
        }
        if (Answer2bool)
        {
            Time.timeScale = 1;
            if (answerButtonText2.text == correctAnswer.ToString())
            {


                if (damaging)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeCorrect;
                    Enemy.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(currentDamage);
                    Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
                }

                Answer1.gameObject.SetActive(false);
                Answer2.gameObject.SetActive(false);
                Answer3.gameObject.SetActive(false);
                noneOfTheAbove.gameObject.SetActive(false);
                Question.gameObject.SetActive(false);
                QuestionaireBG.gameObject.SetActive(false);
                damaging = false;
                Answer2bool = false;
                Cursor.visible = false;
                QuestionType.gameObject.SetActive(false);
            }
            else
            {
                if (damaging)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeWrong;
                }
                Answer1.gameObject.SetActive(false);
                Answer2.gameObject.SetActive(false);
                Answer3.gameObject.SetActive(false);
                noneOfTheAbove.gameObject.SetActive(false);
                Question.gameObject.SetActive(false);
                QuestionaireBG.gameObject.SetActive(false);
                damaging = false;
                Answer2bool = false;
                Cursor.visible = false;
                QuestionType.gameObject.SetActive(false);
            }
        }
        if (Answer3bool)
        {
            Time.timeScale = 1;
            if (answerButtonText3.text == correctAnswer.ToString())
            {

                if (damaging)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeCorrect;
                    Enemy.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(currentDamage);
                    Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
                }

                Answer1.gameObject.SetActive(false);
                Answer2.gameObject.SetActive(false);
                Answer3.gameObject.SetActive(false);
                noneOfTheAbove.gameObject.SetActive(false);
                Question.gameObject.SetActive(false);
                QuestionaireBG.gameObject.SetActive(false);
                damaging = false;
                Answer3bool = false;
                Cursor.visible = false;
                QuestionType.gameObject.SetActive(false);
            }
            else
            {
                if (damaging)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeWrong;
                }
                Answer1.gameObject.SetActive(false);
                Answer2.gameObject.SetActive(false);
                Answer3.gameObject.SetActive(false);
                noneOfTheAbove.gameObject.SetActive(false);
                Question.gameObject.SetActive(false);
                QuestionaireBG.gameObject.SetActive(false);
                damaging = false;
                Answer3bool = false;
                Cursor.visible = false;
                QuestionType.gameObject.SetActive(false);
            }
        }
        if (NFTAbool)
        {
            spacePressed = true;
            Time.timeScale = 1;
            if (correctAnswer.ToString() != answerButtonText1.text)
            {
                if ((correctAnswer.ToString() != answerButtonText2.text))
                {
                    if ((correctAnswer.ToString() != answerButtonText3.text))
                    {

                        if (damaging)
                        {
                            if (spacePressed)
                            {
                                Enemy.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(currentDamage);
                                Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
                                var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                                clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeCorrect;
                            }
                        }

                        Answer1.gameObject.SetActive(false);
                        Answer2.gameObject.SetActive(false);
                        Answer3.gameObject.SetActive(false);
                        noneOfTheAbove.gameObject.SetActive(false);
                        Question.gameObject.SetActive(false);
                        QuestionaireBG.gameObject.SetActive(false);
                        damaging = false;
                        spacePressed = false;
                        NFTAbool = false;
                        Cursor.visible = false;
                        QuestionType.gameObject.SetActive(false);
                    }
                    else
                    {
                        if (spacePressed)
                        {
                            if (damaging)
                            {
                                var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                                clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeWrong;
                            }
                            Answer1.gameObject.SetActive(false);
                            Answer2.gameObject.SetActive(false);
                            Answer3.gameObject.SetActive(false);
                            noneOfTheAbove.gameObject.SetActive(false);
                            Question.gameObject.SetActive(false);
                            QuestionaireBG.gameObject.SetActive(false);
                            damaging = false;
                            spacePressed = false;
                            NFTAbool = false;
                            Cursor.visible = false;
                            QuestionType.gameObject.SetActive(false);
                        }
                    }
                }
                else
                {
                    if (spacePressed)
                    {
                        if (damaging)
                        {
                            var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                            clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeWrong;
                        }
                        Answer1.gameObject.SetActive(false);
                        Answer2.gameObject.SetActive(false);
                        Answer3.gameObject.SetActive(false);
                        noneOfTheAbove.gameObject.SetActive(false);
                        Question.gameObject.SetActive(false);
                        QuestionaireBG.gameObject.SetActive(false);
                        damaging = false;
                        spacePressed = false;
                        NFTAbool = false;
                        Cursor.visible = false;
                        QuestionType.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                if (spacePressed)
                {
                    if (damaging)
                    {
                        var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    }
                    Answer1.gameObject.SetActive(false);
                    Answer2.gameObject.SetActive(false);
                    Answer3.gameObject.SetActive(false);
                    noneOfTheAbove.gameObject.SetActive(false);
                    Question.gameObject.SetActive(false);
                    QuestionaireBG.gameObject.SetActive(false);
                    damaging = false;
                    spacePressed = false;
                    NFTAbool = false;
                    Cursor.visible = false;
                    damaging = false;
                    spacePressed = false;
                    NFTAbool = false;
                    QuestionType.gameObject.SetActive(false);
                }
            }

        }
        if(BossAttacked)
        {
            
            if (PlayerBossAnswer.text == BossAnswer.ToString())
            {
                
                if (damagingBoss)
                {
                    
                    Boss.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(currentDamage);
                    Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeCorrect;
                    BossDamagerActivate = true;
                }

                bossQuestionaire.gameObject.SetActive(false);
                damagingBoss = false;
                Cursor.visible = false;
                Time.timeScale = 1;
                BossCounter = 0;
                BossAttacked = false;
            }
            else
            {
                if (damagingBoss)
                {
                    var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                    clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeWrong;
                }
                bossQuestionaire.gameObject.SetActive(false);
                damagingBoss = false;
                Cursor.visible = false;
                Time.timeScale = 1;
                BossCounter = 0;
                BossAttacked = false;
            }
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Cursor.visible = true;
            currentDamage = damageToGive + thePS.currentATK;
            if(Application.loadedLevel == 7)
            {
                randomOperatorRNG = Random.Range(1, 1);
                Questioner();
            }
            else if(Application.loadedLevel == 8)
            {
                randomOperatorRNG = Random.Range(1, 3);
                Questioner();
            }
            else if (Application.loadedLevel >= 9)
            {
                randomOperatorRNG = Random.Range(1, 4);
                QuestionerTimes();
            }
            else if (Application.loadedLevel >= 10)
            {
                randomOperatorRNG = Random.Range(1, 6);
                QuestionerDivide();
            }
            QuestionType.text = "You Are Attacking!";
            QuestionType.gameObject.SetActive(true);
            damaging = true;
            Enemy = other.gameObject;
            Time.timeScale = 0;
            Answer1.gameObject.SetActive(true);
            Answer2.gameObject.SetActive(true);
            Answer3.gameObject.SetActive(true);
            noneOfTheAbove.gameObject.SetActive(true);
            Question.gameObject.SetActive(true);
            QuestionaireBG.gameObject.SetActive(true);
            if (randomOperatorRNG == 1)
            {
                correctAnswer = randomQuestionaire1 + randomQuestionaire2;
                operatorHolder = "+";
                questionText.text = randomQuestionaire1.ToString() + " " + operatorHolder + " " + randomQuestionaire2.ToString() + " = ? ";
            }
            else if (randomOperatorRNG == 2)
            {
                correctAnswer = randomQuestionaire1 - randomQuestionaire2;
                operatorHolder = "-";
                questionText.text = randomQuestionaire1.ToString() + " " + operatorHolder + " " + randomQuestionaire2.ToString() + " = ? ";
                if (correctAnswer < 0)
                {
                    correctAnswer = randomQuestionaire2 - randomQuestionaire1;
                    questionText.text = randomQuestionaire2.ToString() + " " + operatorHolder + " " + randomQuestionaire1.ToString() + " = ? ";
                }
            }
            else if (randomOperatorRNG == 3)
            {
                correctAnswer = randomQuestionaire1 * randomQuestionaire2;
                operatorHolder = "x";
                questionText.text = randomQuestionaire1.ToString() + " " + operatorHolder + " " + randomQuestionaire2.ToString() + " = ? ";
            }
            else if (randomOperatorRNG >= 4)
            {
                correctAnswer = randomQuestionaire1 / randomQuestionaire2;
                operatorHolder = "÷";
                questionText.text = randomQuestionaire1.ToString() + " " + operatorHolder + " " + randomQuestionaire2.ToString() + " = ? ";
            }
            
            
            answer1RNG = Random.Range(1, 3);
            answer2RNG = Random.Range(1, 3);
            answer3RNG = Random.Range(1, 3);
            correctAnswerRNG = Random.Range(1, 3);
            for (int x = 1; x <= 3; x++)
            {
                if (answer1RNG == answer2RNG)
                {
                    answer1RNG = Random.Range(1, 3);
                }
                else if (answer1RNG == answer3RNG)
                {
                    answer1RNG = Random.Range(1, 3);
                }
                else if (answer2RNG == answer1RNG)
                {
                    answer2RNG = Random.Range(1, 3);
                }
                else if (answer2RNG == answer3RNG)
                {
                    answer2RNG = Random.Range(1, 3);
                }
                else if (answer3RNG == answer1RNG)
                {
                    answer3RNG = Random.Range(1, 3);
                }
                else
                {
                    answer3RNG = correctAnswerRNG;
                }
            }
            for (int x = 1; x <= 3; x++)
            {
                if (correctAnswerRNG == answer1RNG)
                {
                    answerButtonText1.text = correctAnswer.ToString();
                    answerButtonText2.text = randomAnswer.ToString();
                    answerButtonText3.text = randomAnswer2.ToString();
                    x = 4;
                    answer1RNG = 0;
                    answer2RNG = 0;
                    answer3RNG = 0;
                }
                if (correctAnswerRNG == answer2RNG)
                {
                    answerButtonText1.text = randomAnswer.ToString();
                    answerButtonText2.text = correctAnswer.ToString();
                    answerButtonText3.text = randomAnswer2.ToString();
                    x = 4;
                    answer1RNG = 0;
                    answer2RNG = 0;
                    answer3RNG = 0;
                }
                if (correctAnswerRNG == answer3RNG)
                {
                    answerButtonText1.text = randomAnswer.ToString();
                    answerButtonText2.text = randomAnswer2.ToString();
                    answerButtonText3.text = correctAnswer.ToString();
                    x = 4;
                    answer1RNG = 0;
                    answer2RNG = 0;
                    answer3RNG = 0;

                }
            }
        }
        if (other.gameObject.tag == "Boss")
        {
            if (BossDamagerActivate)
            {
                damagingBoss = false;
                Boss.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(currentDamage);
                Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
                var clone = (GameObject)Instantiate(combatText1, hitPoint.position, Quaternion.Euler(Vector3.zero));
                clone.GetComponent<FloatingNumbers>().combatText = combatOutcomeHit;
                BossDamagerActivate = true;
            }
            else
            {
                Cursor.visible = true;
                currentDamage = damageToGive + thePS.currentATK;
                randomOperatorRNG = Random.Range(1, 3);
                bossQuestionaire.gameObject.SetActive(true);
                damagingBoss = true;
                Boss = other.gameObject;
                Time.timeScale = 0;
                randomAnswer = Random.Range(1, 5);
                Cursor.visible = true;
                PlayerBossAnswer.text = "";
                for (int x = 1; x <= 3; x++)
                {
                    if (x == randomOperatorRNG)
                    {
                        BossCounter++;
                    }

                    if (BossCounter == 1)
                    {
                        BossQuestionHolder = randomAnswer;
                        randomAnswer += randomAnswer;
                        BossQuestion = randomAnswer;
                        BossRandom1.text = BossQuestionHolder + " + " + BossQuestionHolder + " = " + BossQuestion;
                        BossCounter++;

                    }
                    if (BossCounter == 2)
                    {
                        BossQuestionHolder = randomAnswer;
                        randomAnswer += randomAnswer;
                        BossQuestion = randomAnswer;
                        BossRandom2.text = BossQuestionHolder + " + " + BossQuestionHolder + " = ?";
                        BossAnswer = randomAnswer;
                        BossCounter++;
                    }
                    if (BossCounter == 3)
                    {
                        BossQuestionHolder = randomAnswer;
                        randomAnswer += randomAnswer;
                        BossQuestion = randomAnswer;
                        BossRandom3.text = BossQuestionHolder + " + " + BossQuestionHolder + " = " + BossQuestion;
                        BossCounter = 0;
                    }
                }
            }
            

        }
        if (other.gameObject.tag == "Quest")
        {

        }
            //other.gameObject.GetComponent<EnemyHealthSystem>().HurtEnemy(damageToGive);
            //Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
        }
    void Questioner()
    {
        randomAnswer = Random.Range(1, 25);
        randomAnswer2 = Random.Range(1, 25);
        randomQuestionaire1 = Random.Range(1, 15);
        randomQuestionaire2 = Random.Range(1, 15);
    }
    void QuestionerTimes()
    {
        randomAnswer = Random.Range(1, 25);
        randomAnswer2 = Random.Range(1, 25);
        randomQuestionaire1 = Random.Range(1, 5);
        randomQuestionaire2 = Random.Range(1, 5);
    }
    void QuestionerDivide()
    {
        randomAnswer = Random.Range(1, 5);
        randomAnswer2 = Random.Range(1, 5);
        randomQuestionaire1 = Random.Range(1, 25);
        randomQuestionaire2 = Random.Range(1, 25);
    }

    public void Answer1st()
    {
        Answer1bool = true;
    }
    public void Answer2ns()
    {
        Answer2bool = true;
    }
    public void Answer3rd()
    {
        Answer3bool = true;
    }
    public void NoftheA()
    {
        NFTAbool = true;
    }
    public void BossAttack()
    {
        BossAttacked = true;
    }
}
