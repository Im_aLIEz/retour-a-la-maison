﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    

    public int currentAchievement;

    public int[] toRankUp;
    public int[] HPRanks;
    public int[] ATKRanks;

    public int currentRank;
    public int currentHP;
    public int currentATK;
    public int currentKills;

    private PlayerHealthSystem thePlayerHealth;

    // Use this for initialization
    void Start() {
        currentHP = HPRanks[0];
        currentATK = ATKRanks[0];

        thePlayerHealth = FindObjectOfType<PlayerHealthSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentAchievement >= toRankUp[currentRank])
        {
            //currentRank++;
        }
        
    }

    public void AddAchievement(int achievementToAdd)
    {
        currentAchievement = PlayerPrefs.GetInt("Points");
        currentAchievement += achievementToAdd;
        PlayerPrefs.SetInt("Points", currentAchievement);
        currentKills = PlayerPrefs.GetInt("CurrentKills");
        currentKills++;
        PlayerPrefs.SetInt("CurrentKills", currentKills);
    }

    public void RankUp()
    {
        currentRank++;
    }

}
