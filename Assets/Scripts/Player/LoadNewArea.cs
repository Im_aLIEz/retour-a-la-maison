﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNewArea : MonoBehaviour {

    public string levelToLoad;

    public string exitPoint;

    private PlayerController thePlayer;
    private PlayerHealthSystem pHealth;
	// Use this for initialization
	void Start () {
        thePlayer = FindObjectOfType<PlayerController>();
        pHealth = FindObjectOfType<PlayerHealthSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
            
            thePlayer.startPoint = exitPoint;
            Application.LoadLevel(levelToLoad);
            pHealth.StageStart();
        }
    }
}
