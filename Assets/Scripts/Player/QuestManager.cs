﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour {

    private int currentKills;
    public int neededKills;

    public Text Quest;
    public GameObject nextArea;
    public GameObject doorLocker;

    public GameObject bossLocker;

    public bool FindtheMissing;
    public Image FindtheMissingQuest;

    public string Answer;

    public bool Boss;
    // Use this for initialization
    void Start () {
        PlayerPrefs.SetInt("CurrentKills", 0);
	}
	
	// Update is called once per frame
	void Update () {
        currentKills = PlayerPrefs.GetInt("CurrentKills");
        if(!Boss)
        {
            Quest.text = "Monsters to Slay: " + currentKills + "/" + neededKills;
        }
        if(Boss)
        {
            Quest.text = "Monsters to Slay: " + currentKills + "/" + (neededKills-1);
        }
        
        if(currentKills==neededKills)
        {
            nextArea.gameObject.SetActive(true);
            doorLocker.gameObject.SetActive(false);
            Quest.text = "Completed";
            FindtheMissingQuest.gameObject.SetActive(false);
        }
        else
        {
            doorLocker.gameObject.SetActive(true);
            nextArea.gameObject.SetActive(false);
        }
        if(currentKills == neededKills - 1)
        {
            if(Boss)
            {
                Quest.text = "Slay the Boss";
            }
            bossLocker.gameObject.SetActive(false);
            if(FindtheMissing)
            {
                FindtheMissingQuest.gameObject.SetActive(true);
            }
        }
    }
}
