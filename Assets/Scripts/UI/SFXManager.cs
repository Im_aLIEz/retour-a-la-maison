﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour {

    public AudioSource PlayerHurt;
    public AudioSource PlayerDead;
    public AudioSource PlayerAttack;
    public AudioSource QuestDone;
    public AudioSource Direction;
    public AudioSource PlayerBlock;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
