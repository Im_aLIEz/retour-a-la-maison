﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour 
{
    public GameObject Thing;

    public GameObject Player;
    public GameObject Canvas;
    public void ChangeToScene (string sceneToChangeTo) 
	{
        if(sceneToChangeTo=="Main Menu")
        {
            SceneManager.LoadScene(sceneToChangeTo);
            Thing.gameObject.SetActive(false);
            Destroy(Player);
            Destroy(Canvas);
        }
        else
        {
            SceneManager.LoadScene(sceneToChangeTo);
            Thing.gameObject.SetActive(false);
            Player.gameObject.SetActive(true);
            Canvas.gameObject.SetActive(true);
        }
        

        
            
        Time.timeScale = 1;
	}
}
