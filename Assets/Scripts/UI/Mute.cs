﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mute : MonoBehaviour {

    public Text Mutebtn;

    public Text Highscore;

    int totalHighscore;
    int muteCounter;

    public GameObject Correction;

    public void Mutes()
    {
        muteCounter++;
        
        
    }

    void Update()
    {
        if (muteCounter == 1)
        {
            AudioListener.pause = !AudioListener.pause; ;
            Mutebtn.text = "Mute";
        }
        else if (muteCounter == 2)
        {
            AudioListener.pause = !AudioListener.pause; ;
            Mutebtn.text = "Unmute";
            muteCounter = 0;
        }
        totalHighscore = PlayerPrefs.GetInt("Points");
        Highscore.text = "Highscore: " + totalHighscore;

    }
    public void ResetButton()
    {
        PlayerPrefs.SetInt("Points", 0);
        PlayerPrefs.SetInt("Progress", 0);
        Correction.gameObject.SetActive(false);
    }

        

}
