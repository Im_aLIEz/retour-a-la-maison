﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayHighscore : MonoBehaviour {

    public Text Points;
    private int TotalPoints;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        TotalPoints = PlayerPrefs.GetInt("Points");
        Points.text = "High Score: " + TotalPoints;
    }
}
