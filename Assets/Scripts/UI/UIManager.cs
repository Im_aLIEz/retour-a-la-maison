﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Slider healthBar;
    public Text healthText;
    public PlayerHealthSystem playerHealth;
    public static bool uiExists;

    public GameObject GamePause;
    public Text Points;

    private int currentKills;
    private int GPexists;
    private int TotalPoints;

	// Use this for initialization
	void Start () {
        if (!uiExists)
        {   
            uiExists = true;
            DontDestroyOnLoad(transform.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        healthBar.maxValue = playerHealth.playerMaxHealth;
        healthBar.value = playerHealth.playerCurrentHealth;
        healthText.text = "HP: " + playerHealth.playerCurrentHealth + "/" + playerHealth.playerMaxHealth;
        TotalPoints = PlayerPrefs.GetInt("Points");
        Points.text = "High Score: " + TotalPoints;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            GPexists++;
            if(GPexists == 1)
            {
                GamePause.gameObject.SetActive(true);
                Time.timeScale = 0;
                Cursor.visible = true;
            }
            if (GPexists == 2)
            {
                Cursor.visible = false;
                GamePause.gameObject.SetActive(false);
                GPexists = 0;
                Time.timeScale = 1;
            }

        }
	}


}
