﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthSystem : MonoBehaviour {

    public int MaxHealth;
    public int CurrentHealth;

    public GameObject coin;
    public Transform droppoint;
    private PlayerStats thePlayerStats;

    public int achievementsToGive;

    public bool Quest;

    public string mobAnswer;
    public Text mobNumberText;

    private QuestManager theQuest;
    private PlayerHealthSystem pHealth;

    private PlayerAttack Theplayer;
    // Use this for initialization
    void Start()
    {
        CurrentHealth = MaxHealth;
        thePlayerStats = FindObjectOfType<PlayerStats>();
        theQuest = FindObjectOfType<QuestManager>();
        mobNumberText.text = mobAnswer;
        pHealth = FindObjectOfType<PlayerHealthSystem>();
        Theplayer = FindObjectOfType<PlayerAttack>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentHealth <= 0)
        {
            mobNumberText.text = mobAnswer;
            Destroy(gameObject);
            thePlayerStats.AddAchievement(achievementsToGive);
            Instantiate(coin, droppoint.position, droppoint.rotation);
            Theplayer.BossDamagerActivate = false;
        }
    }
    public void HurtEnemy(int damageToGive)
    {
        if(!Quest)
        {
            CurrentHealth -= damageToGive;
        }
        else if(Quest)
        {
            if (mobAnswer == theQuest.Answer)
            {
                CurrentHealth -= damageToGive;
            }
            else
            {
                pHealth.QuestDamager();
            }
        }
    }
    public void SetMaxHealth()
    {
        CurrentHealth = MaxHealth;
    }
}
